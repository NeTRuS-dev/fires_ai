import os
from tensorflow.keras.models import Sequential
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.layers import Dense, BatchNormalization
from tensorflow.keras.losses import mean_absolute_percentage_error
from tensorflow.keras.activations import relu
from tensorflow import keras

model = None
if os.path.exists('./model'):
    model = keras.models.load_model('./model')
else:
    model = Sequential()

    model.add(Dense(4, activation=relu))
    model.add(Dense(4096, activation=relu))
    model.add(BatchNormalization())
    model.add(Dense(4096, activation=relu))
    model.add(Dense(4096, activation=relu))
    model.add(BatchNormalization())
    model.add(Dense(2048, activation=relu))
    model.add(Dense(1024, activation=relu))
    model.add(Dense(128, activation=relu))

    model.add(Dense(4, activation=None))

model.compile(optimizer=Adam(),
              loss=mean_absolute_percentage_error,
              metrics=['acc', 'mse'])
