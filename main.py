import pandas as pd
import numpy as np
from train_data import x_train, y_train, make_train, get_bounding_box, get_polygon_from_points
from model import model
import geopandas as gpd
import matplotlib.pyplot as plt
import contextily as cx

def get_predict(predict_model, df):
    out_df = np.empty((0, 2))
    for idx, data in df.iterrows():
        coords_in = get_bounding_box(data['geometry'])
        result = predict_model.predict(np.array([list(coords_in)]))
        poly = get_polygon_from_points(*(result[0, :]))
        out_df = np.append(out_df, np.array([[data['fireid'], poly]]), axis=0)
    return pd.DataFrame(out_df, columns=['fireid', 'geometry'])


if __name__ == '__main__':
    fig, ax = plt.subplots(1, 1)
    vdf = gpd.read_file('input/validate_public.shp')
    vdf.crs = "epsg:4326"
    vdf.to_crs(epsg=4326)
    vdf.plot(figsize=(10, 10), alpha=0.5, color='blue', ax=ax)
    # dt = pd.read_csv('fires_growth/final/vdi.csv', index_col=0)
    # # dt = pd.read_csv('fires_growth/final/vdi.csv', index_col=0)
    rdf = get_predict(model, vdf)
    # rdf.to_csv('fires_growth/final/results.csv')
    grdf = gpd.GeoDataFrame(rdf, geometry=rdf['geometry'])
    grdf.crs = "epsg:4326"
    grdf.to_crs(epsg=4326)
    grdf.to_file('output/result.shp')
    cx.add_basemap(ax, crs=grdf.crs)
    plt.show()
    # make_train(model, x_train, y_train, 13)
