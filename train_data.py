import os
import pickle
from shapely import wkt
import pandas as pd
import numpy as np
from shapely.geometry import box
from matplotlib import pyplot as plt


def get_bounding_box(poly_str):
    p = poly_str
    if isinstance(poly_str, str):
        p = wkt.loads(poly_str)

    return p.bounds


def get_polygon_from_points(minx, miny, maxx, maxy):
    return box(minx, miny, maxx, maxy)


x_train = None
y_train = None

if os.path.exists('train/x_train.pickle'):
    with open('train/x_train.pickle', 'rb') as f:
        x_train = pickle.load(f)

if os.path.exists('train/y_train.pickle'):
    with open('train/y_train.pickle', 'rb') as f:
        y_train = pickle.load(f)

if x_train is None or y_train is None:
    # TODO GEOPANDAS
    dt = pd.read_csv('fires_growth/prepared_fires.csv', index_col=0)
    dt.loc[:, 'geometry'] = dt['active_fir']
    x_train = np.empty((0, 4))
    y_train = np.empty((0, 4))
    for fireid in dt['fireid'].unique():
        measures = dt.loc[dt['fireid'] == fireid].dropna().sort_values(['dt'])
        for duration in measures['duration'].unique():
            duration_measures = measures[measures['duration'] == duration]
            duration_measures = [v for i, v in duration_measures.iterrows()]
            for idx, fire_start in enumerate(duration_measures[:-1]):
                x_train = np.append(x_train, np.array([[
                    *get_bounding_box(fire_start['geometry']),
                ]]), axis=0)
                y_train = np.append(y_train,
                                    np.array([list(get_bounding_box(duration_measures[idx + 1]['geometry']))]),
                                    axis=0)

with open('train/x_train.pickle', 'wb') as f:
    pickle.dump(x_train, f)
with open('train/y_train.pickle', 'wb') as f:
    pickle.dump(y_train, f)


def make_train(model, x, y, epochs):
    history = model.fit(
        x,
        y,
        batch_size=32,
        validation_split=0.2,
        epochs=epochs,
    )
    plt.plot(history.history['loss'], label='loss')
    plt.plot(history.history['val_loss'], label='val_loss')
    plt.legend()
    plt.show()

    model.save('./model')
